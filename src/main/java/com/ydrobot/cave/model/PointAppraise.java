package com.ydrobot.cave.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 部件评价记录表
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@TableName("point_appraise")
@ApiModel(value="PointAppraise对象", description="部件评价记录表")
public class PointAppraise implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "设备id")
    private Integer deviceId;

    @ApiModelProperty(value = "部件名称")
    private String name;

    @ApiModelProperty(value = "评价状态")
    private String appraiseStatus;

    @ApiModelProperty(value = "评价时间")
    private Date appraiseTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppraiseStatus() {
        return appraiseStatus;
    }

    public void setAppraiseStatus(String appraiseStatus) {
        this.appraiseStatus = appraiseStatus;
    }

    public Date getAppraiseTime() {
        return appraiseTime;
    }

    public void setAppraiseTime(Date appraiseTime) {
        this.appraiseTime = appraiseTime;
    }

    @Override
    public String toString() {
        return "PointAppraise{" +
        "id=" + id +
        ", deviceId=" + deviceId +
        ", name=" + name +
        ", appraiseStatus=" + appraiseStatus +
        ", appraiseTime=" + appraiseTime +
        "}";
    }
}
