package com.ydrobot.cave.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 设备运行表
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@TableName("device_run")
@ApiModel(value="DeviceRun对象", description="设备运行表")
public class DeviceRun implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "设备id")
    private Integer deviceId;

    @ApiModelProperty(value = "负载率")
    private String loadRate;

    @ApiModelProperty(value = "当前有功")
    private String activePower;

    @ApiModelProperty(value = "今日调整档位次数")
    private String gearNum;

    @ApiModelProperty(value = "今日投切电容器次数")
    private String capacitorNum;

    @ApiModelProperty(value = "今日投切电抗器次数")
    private String reactorNum;

    @ApiModelProperty(value = "当前油温")
    private String oilTemp;

    @ApiModelProperty(value = "重过载情况")
    private String heavyOverload;

    @ApiModelProperty(value = "日重载小时数")
    private String dailyHeavyLoad;

    @ApiModelProperty(value = "日过载小时数")
    private String dailyOverload;

    @ApiModelProperty(value = "刷新时间")
    private Date refreshTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getLoadRate() {
        return loadRate;
    }

    public void setLoadRate(String loadRate) {
        this.loadRate = loadRate;
    }

    public String getActivePower() {
        return activePower;
    }

    public void setActivePower(String activePower) {
        this.activePower = activePower;
    }

    public String getGearNum() {
        return gearNum;
    }

    public void setGearNum(String gearNum) {
        this.gearNum = gearNum;
    }

    public String getCapacitorNum() {
        return capacitorNum;
    }

    public void setCapacitorNum(String capacitorNum) {
        this.capacitorNum = capacitorNum;
    }

    public String getReactorNum() {
        return reactorNum;
    }

    public void setReactorNum(String reactorNum) {
        this.reactorNum = reactorNum;
    }

    public String getOilTemp() {
        return oilTemp;
    }

    public void setOilTemp(String oilTemp) {
        this.oilTemp = oilTemp;
    }

    public String getHeavyOverload() {
        return heavyOverload;
    }

    public void setHeavyOverload(String heavyOverload) {
        this.heavyOverload = heavyOverload;
    }

    public String getDailyHeavyLoad() {
        return dailyHeavyLoad;
    }

    public void setDailyHeavyLoad(String dailyHeavyLoad) {
        this.dailyHeavyLoad = dailyHeavyLoad;
    }

    public String getDailyOverload() {
        return dailyOverload;
    }

    public void setDailyOverload(String dailyOverload) {
        this.dailyOverload = dailyOverload;
    }

    public Date getRefreshTime() {
        return refreshTime;
    }

    public void setRefreshTime(Date refreshTime) {
        this.refreshTime = refreshTime;
    }

    @Override
    public String toString() {
        return "DeviceRun{" +
        "id=" + id +
        ", deviceId=" + deviceId +
        ", loadRate=" + loadRate +
        ", activePower=" + activePower +
        ", gearNum=" + gearNum +
        ", capacitorNum=" + capacitorNum +
        ", reactorNum=" + reactorNum +
        ", oilTemp=" + oilTemp +
        ", heavyOverload=" + heavyOverload +
        ", dailyHeavyLoad=" + dailyHeavyLoad +
        ", dailyOverload=" + dailyOverload +
        ", refreshTime=" + refreshTime +
        "}";
    }
}
