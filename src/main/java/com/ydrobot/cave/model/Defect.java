package com.ydrobot.cave.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 设备缺陷表
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@TableName("defect")
@ApiModel(value="Defect对象", description="设备缺陷表")
public class Defect implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "设备id")
    private Integer deviceId;

    @ApiModelProperty(value = "部件名称")
    private String pointName;

    @ApiModelProperty(value = "部件类型")
    private String pointType;

    @ApiModelProperty(value = "部位")
    private String position;

    @ApiModelProperty(value = "缺陷描述")
    private String description;

    @ApiModelProperty(value = "缺陷程度")
    private String degree;

    @ApiModelProperty(value = "缺陷内容")
    private String content;

    @ApiModelProperty(value = "来源")
    private String source;

    @ApiModelProperty(value = "天气")
    private String weather;

    @ApiModelProperty(value = "发现时间")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Defect{" +
        "id=" + id +
        ", deviceId=" + deviceId +
        ", pointName=" + pointName +
        ", pointType=" + pointType +
        ", position=" + position +
        ", description=" + description +
        ", degree=" + degree +
        ", content=" + content +
        ", source=" + source +
        ", weather=" + weather +
        ", createTime=" + createTime +
        "}";
    }
}
