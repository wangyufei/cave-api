package com.ydrobot.cave.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 设备信息表
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@TableName("device")
@ApiModel(value="Device对象", description="设备信息表")
public class Device implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "设备名称")
    private String deviceName;

    @ApiModelProperty(value = "变电站名称")
    private String stationName;

    @ApiModelProperty(value = "设备类型")
    private String deviceType;

    @ApiModelProperty(value = "电压等级")
    private String voltageGrade;

    @ApiModelProperty(value = "相数")
    private String phaseNum;

    @ApiModelProperty(value = "所属地市")
    private String localCity;

    @ApiModelProperty(value = "绝缘耐热等级")
    private String insulationClass;

    @ApiModelProperty(value = "检修人")
    private String maintenancePersonnel;

    @ApiModelProperty(value = "变压器容量")
    private String transformerCapacity;

    @ApiModelProperty(value = "生产厂家")
    private String manufacturer;

    @ApiModelProperty(value = "周波")
    private String cycle;

    @ApiModelProperty(value = "送电时间")
    private Date powerTransmissionTime;

    @ApiModelProperty(value = "重要性等级")
    private String importanceLevel;

    @ApiModelProperty(value = "上次检修时间")
    private Date lastOverhaulTime;

    @ApiModelProperty(value = "评价状态")
    private String appraiseStatus;

    @ApiModelProperty(value = "评价正常占比")
    private Integer appraiseNormalProportion;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getVoltageGrade() {
        return voltageGrade;
    }

    public void setVoltageGrade(String voltageGrade) {
        this.voltageGrade = voltageGrade;
    }

    public String getPhaseNum() {
        return phaseNum;
    }

    public void setPhaseNum(String phaseNum) {
        this.phaseNum = phaseNum;
    }

    public String getLocalCity() {
        return localCity;
    }

    public void setLocalCity(String localCity) {
        this.localCity = localCity;
    }

    public String getInsulationClass() {
        return insulationClass;
    }

    public void setInsulationClass(String insulationClass) {
        this.insulationClass = insulationClass;
    }

    public String getMaintenancePersonnel() {
        return maintenancePersonnel;
    }

    public void setMaintenancePersonnel(String maintenancePersonnel) {
        this.maintenancePersonnel = maintenancePersonnel;
    }

    public String getTransformerCapacity() {
        return transformerCapacity;
    }

    public void setTransformerCapacity(String transformerCapacity) {
        this.transformerCapacity = transformerCapacity;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public Date getPowerTransmissionTime() {
        return powerTransmissionTime;
    }

    public void setPowerTransmissionTime(Date powerTransmissionTime) {
        this.powerTransmissionTime = powerTransmissionTime;
    }

    public String getImportanceLevel() {
        return importanceLevel;
    }

    public void setImportanceLevel(String importanceLevel) {
        this.importanceLevel = importanceLevel;
    }

    public Date getLastOverhaulTime() {
        return lastOverhaulTime;
    }

    public void setLastOverhaulTime(Date lastOverhaulTime) {
        this.lastOverhaulTime = lastOverhaulTime;
    }

    public String getAppraiseStatus() {
        return appraiseStatus;
    }

    public void setAppraiseStatus(String appraiseStatus) {
        this.appraiseStatus = appraiseStatus;
    }

    public Integer getAppraiseNormalProportion() {
        return appraiseNormalProportion;
    }

    public void setAppraiseNormalProportion(Integer appraiseNormalProportion) {
        this.appraiseNormalProportion = appraiseNormalProportion;
    }

    @Override
    public String toString() {
        return "Device{" +
        "id=" + id +
        ", deviceName=" + deviceName +
        ", stationName=" + stationName +
        ", deviceType=" + deviceType +
        ", voltageGrade=" + voltageGrade +
        ", phaseNum=" + phaseNum +
        ", localCity=" + localCity +
        ", insulationClass=" + insulationClass +
        ", maintenancePersonnel=" + maintenancePersonnel +
        ", transformerCapacity=" + transformerCapacity +
        ", manufacturer=" + manufacturer +
        ", cycle=" + cycle +
        ", powerTransmissionTime=" + powerTransmissionTime +
        ", importanceLevel=" + importanceLevel +
        ", lastOverhaulTime=" + lastOverhaulTime +
        ", appraiseStatus=" + appraiseStatus +
        ", appraiseNormalProportion=" + appraiseNormalProportion +
        "}";
    }
}
