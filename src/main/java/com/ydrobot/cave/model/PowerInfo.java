package com.ydrobot.cave.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 电源信息表
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@TableName("power_info")
@ApiModel(value="PowerInfo对象", description="电源信息表")
public class PowerInfo implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "设备id")
    private Integer deviceId;

    @ApiModelProperty(value = "电源类型")
    private String type;

    @ApiModelProperty(value = "线路名称")
    private String lineName;

    @ApiModelProperty(value = "变电站名称")
    private String stationName;

    @ApiModelProperty(value = "电源槽数")
    private String powerSlots;

    @ApiModelProperty(value = "供电电压")
    private String supplyVoltage;

    @ApiModelProperty(value = "供电容量")
    private String supplyCapacity;

    @ApiModelProperty(value = "电缆敷设")
    private String cableLaying;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getPowerSlots() {
        return powerSlots;
    }

    public void setPowerSlots(String powerSlots) {
        this.powerSlots = powerSlots;
    }

    public String getSupplyVoltage() {
        return supplyVoltage;
    }

    public void setSupplyVoltage(String supplyVoltage) {
        this.supplyVoltage = supplyVoltage;
    }

    public String getSupplyCapacity() {
        return supplyCapacity;
    }

    public void setSupplyCapacity(String supplyCapacity) {
        this.supplyCapacity = supplyCapacity;
    }

    public String getCableLaying() {
        return cableLaying;
    }

    public void setCableLaying(String cableLaying) {
        this.cableLaying = cableLaying;
    }

    @Override
    public String toString() {
        return "PowerInfo{" +
        "id=" + id +
        ", deviceId=" + deviceId +
        ", type=" + type +
        ", lineName=" + lineName +
        ", stationName=" + stationName +
        ", powerSlots=" + powerSlots +
        ", supplyVoltage=" + supplyVoltage +
        ", supplyCapacity=" + supplyCapacity +
        ", cableLaying=" + cableLaying +
        "}";
    }
}
