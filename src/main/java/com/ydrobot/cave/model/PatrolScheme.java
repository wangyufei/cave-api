package com.ydrobot.cave.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 巡检计划表
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@TableName("patrol_scheme")
@ApiModel(value="PatrolScheme对象", description="巡检计划表")
public class PatrolScheme implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "设备id")
    private Integer deviceId;

    @ApiModelProperty(value = "所属城市")
    private String localCity;

    @ApiModelProperty(value = "编制单位")
    private String organizationUnit;

    @ApiModelProperty(value = "专业")
    private String speciality;

    @ApiModelProperty(value = "电站线路")
    private String powerLine;

    @ApiModelProperty(value = "电压等级")
    private String voltageGrade;

    @ApiModelProperty(value = "是否停电")
    private String isPowerCut;

    @ApiModelProperty(value = "停电范围")
    private String powerCutRange;

    @ApiModelProperty(value = "工作类型")
    private String workType;

    @ApiModelProperty(value = "工作内容")
    private String workContent;

    @ApiModelProperty(value = "计划编制时间")
    private String createTime;

    @ApiModelProperty(value = "计划开始时间")
    private String startTime;

    @ApiModelProperty(value = "计划完成时间")
    private String endTime;

    @ApiModelProperty(value = "计划状态")
    private String status;

    @ApiModelProperty(value = "来源")
    private String source;

    @ApiModelProperty(value = "调度是否批复")
    private String isReply;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getLocalCity() {
        return localCity;
    }

    public void setLocalCity(String localCity) {
        this.localCity = localCity;
    }

    public String getOrganizationUnit() {
        return organizationUnit;
    }

    public void setOrganizationUnit(String organizationUnit) {
        this.organizationUnit = organizationUnit;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getPowerLine() {
        return powerLine;
    }

    public void setPowerLine(String powerLine) {
        this.powerLine = powerLine;
    }

    public String getVoltageGrade() {
        return voltageGrade;
    }

    public void setVoltageGrade(String voltageGrade) {
        this.voltageGrade = voltageGrade;
    }

    public String getIsPowerCut() {
        return isPowerCut;
    }

    public void setIsPowerCut(String isPowerCut) {
        this.isPowerCut = isPowerCut;
    }

    public String getPowerCutRange() {
        return powerCutRange;
    }

    public void setPowerCutRange(String powerCutRange) {
        this.powerCutRange = powerCutRange;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getWorkContent() {
        return workContent;
    }

    public void setWorkContent(String workContent) {
        this.workContent = workContent;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getIsReply() {
        return isReply;
    }

    public void setIsReply(String isReply) {
        this.isReply = isReply;
    }

    @Override
    public String toString() {
        return "PatrolScheme{" +
        "id=" + id +
        ", deviceId=" + deviceId +
        ", localCity=" + localCity +
        ", organizationUnit=" + organizationUnit +
        ", speciality=" + speciality +
        ", powerLine=" + powerLine +
        ", voltageGrade=" + voltageGrade +
        ", isPowerCut=" + isPowerCut +
        ", powerCutRange=" + powerCutRange +
        ", workType=" + workType +
        ", workContent=" + workContent +
        ", createTime=" + createTime +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", status=" + status +
        ", source=" + source +
        ", isReply=" + isReply +
        "}";
    }
}
