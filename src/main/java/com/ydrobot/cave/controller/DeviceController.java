package com.ydrobot.cave.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.ydrobot.cave.common.api.CommonResult;
import com.ydrobot.cave.model.Defect;
import com.ydrobot.cave.model.Device;
import com.ydrobot.cave.model.DeviceRun;
import com.ydrobot.cave.model.PatrolScheme;
import com.ydrobot.cave.model.PointAppraise;
import com.ydrobot.cave.model.PowerInfo;
import com.ydrobot.cave.service.DefectService;
import com.ydrobot.cave.service.DeviceRunService;
import com.ydrobot.cave.service.DeviceService;
import com.ydrobot.cave.service.PatrolSchemeService;
import com.ydrobot.cave.service.PointAppraiseService;
import com.ydrobot.cave.service.PowerInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 设备信息表 前端控制器
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@RestController
@Api(tags = "设备管理模块")
@RequestMapping("/devices")
public class DeviceController {
	
	@Autowired
	private DeviceService deviceService;
	@Autowired
	private PowerInfoService powerInfoService;
	@Autowired
	private PointAppraiseService pointAppraiseService;
	@Autowired
	private DeviceRunService deviceRunService;
	@Autowired
	private PatrolSchemeService patrolSchemeService;
	@Autowired
	private DefectService defectService;
	
	@ApiOperation("获取指定设备信息")
	@GetMapping("/{id}")
	public CommonResult<Device> getItem(@PathVariable Integer id) {
		Device device = deviceService.getById(id);
		return CommonResult.success(device);
	}
	
	@ApiOperation("获取指定设备供电电源信息")
	@GetMapping("/{id}/powerInfo")
	public CommonResult<List<PowerInfo>> getPowerInfo(@PathVariable Integer id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("device_id", id);
		List<PowerInfo> list = powerInfoService.listByMap(map);
		return CommonResult.success(list);
	}
	
	@ApiOperation("获取指定设备部件状态信息")
	@GetMapping("/{id}/pointAppraise")
	public CommonResult<List<PointAppraise>> getPointAppraise(@PathVariable Integer id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("device_id", id);
		List<PointAppraise> list = pointAppraiseService.listByMap(map);
		return CommonResult.success(list);
	}
	
	@ApiOperation("获取指定设备运行信息")
	@GetMapping("/{id}/runInfo")
	public CommonResult<List<DeviceRun>> getRunInfo(@PathVariable Integer id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("device_id", id);
		List<DeviceRun> list = deviceRunService.listByMap(map);
		return CommonResult.success(list);
	}
	
	@ApiOperation("获取指定设备巡检计划")
	@GetMapping("/{id}/patrolScheme")
	public CommonResult<List<PatrolScheme>> getPatrolScheme(@PathVariable Integer id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("device_id", id);
		List<PatrolScheme> list = patrolSchemeService.listByMap(map);
		return CommonResult.success(list);
	}
	
	@ApiOperation("获取指定设备缺陷信息")
	@GetMapping("/{id}/defect")
	public CommonResult<List<Defect>> getDefect(@PathVariable Integer id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("device_id", id);
		List<Defect> list = defectService.listByMap(map);
		return CommonResult.success(list);
	}
}

