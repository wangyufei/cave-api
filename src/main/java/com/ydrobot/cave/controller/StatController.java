package com.ydrobot.cave.controller;


import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ydrobot.cave.common.api.CommonResult;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 设备缺陷表 前端控制器
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@RestController
@Api(tags = "统计管理模块")
@RequestMapping("/stat")
public class StatController {

	@ApiOperation("获取负荷曲线")
	@GetMapping("/countLoad")
	@ApiImplicitParam(name="format",value="数据格式，m->30天，h->24小时",dataType="String", paramType = "query")
	public CommonResult<Map<String, Object>> countLoad(
			@RequestParam String format) {

		if (StrUtil.isEmpty(format)) {
			format = "h";
		}

		Map<String, Object> map = new HashMap<String, Object>();
		if ("h".equals(format)) {
			for (int i = 1; i <= 24; i++) {
				Integer value = getLoadValue();
				map.put(String.valueOf(i), value);
			}
		}

		if ("m".equals(format)) {
			for (int i = 1; i <= 30; i++) {
				Integer value = getLoadValue();
				map.put(String.valueOf(i), value);
			}
		}

		return CommonResult.success(map);
	}
	
	@ApiOperation("获取油温曲线")
	@GetMapping("/countOilTemp")
	@ApiImplicitParam(name="format",value="数据格式，m->30天，h->24小时",dataType="String", paramType = "query")
	public CommonResult<Map<String, Object>> countOilTemp(
			@RequestParam String format) {

		if (StrUtil.isEmpty(format)) {
			format = "h";
		}

		Map<String, Object> map = new HashMap<String, Object>();
		if ("h".equals(format)) {
			for (int i = 1; i <= 24; i++) {
				Integer value = getOilTempValue();
				map.put(String.valueOf(i), value);
			}
		}

		if ("m".equals(format)) {
			for (int i = 1; i <= 30; i++) {
				Integer value = getOilTempValue();
				map.put(String.valueOf(i), value);
			}
		}

		return CommonResult.success(map);
	}
	
	@ApiOperation("获取绕组温度曲线")
	@GetMapping("/countWindingTemp")
	@ApiImplicitParam(name="format",value="数据格式，m->30天，h->24小时",dataType="String", paramType = "query")
	public CommonResult<Map<String, Object>> countWindingTemp(
			@RequestParam String format) {

		if (StrUtil.isEmpty(format)) {
			format = "h";
		}

		Map<String, Object> map = new HashMap<String, Object>();
		if ("h".equals(format)) {
			for (int i = 1; i <= 24; i++) {
				Integer value = getWindingTempValue();
				map.put(String.valueOf(i), value);
			}
		}

		if ("m".equals(format)) {
			for (int i = 1; i <= 30; i++) {
				Integer value = getWindingTempValue();
				map.put(String.valueOf(i), value);
			}
		}

		return CommonResult.success(map);
	}

	private Integer getLoadValue() {
		Integer curH = DateUtil.thisHour(true);
		Integer value = 0;
		if (curH >= 9 && curH <= 23) {
			value = (int) (Math.random() * 10 + 80);
		} else {
			value = (int) (Math.random() * 10 + 60);
		}

		return value;
	}
	
	private Integer getOilTempValue() {
		Integer value = (int) (Math.random() * 4 + 28);
		return value;
	}
	
	private Integer getWindingTempValue() {
		Integer value = (int) (Math.random() * 3 + 14);
		return value;
	}
}
