package com.ydrobot.cave.mapper;

import com.ydrobot.cave.model.PatrolScheme;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡检计划表 Mapper 接口
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface PatrolSchemeMapper extends BaseMapper<PatrolScheme> {

}
