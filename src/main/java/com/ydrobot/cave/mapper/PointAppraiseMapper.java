package com.ydrobot.cave.mapper;

import com.ydrobot.cave.model.PointAppraise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部件评价记录表 Mapper 接口
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface PointAppraiseMapper extends BaseMapper<PointAppraise> {

}
