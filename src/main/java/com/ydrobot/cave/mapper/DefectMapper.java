package com.ydrobot.cave.mapper;

import com.ydrobot.cave.model.Defect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设备缺陷表 Mapper 接口
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface DefectMapper extends BaseMapper<Defect> {

}
