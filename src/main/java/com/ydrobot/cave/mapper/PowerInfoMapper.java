package com.ydrobot.cave.mapper;

import com.ydrobot.cave.model.PowerInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 电源信息表 Mapper 接口
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface PowerInfoMapper extends BaseMapper<PowerInfo> {

}
