package com.ydrobot.cave.mapper;

import com.ydrobot.cave.model.Device;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设备信息表 Mapper 接口
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface DeviceMapper extends BaseMapper<Device> {

}
