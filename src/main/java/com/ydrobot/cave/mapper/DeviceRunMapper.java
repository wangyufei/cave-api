package com.ydrobot.cave.mapper;

import com.ydrobot.cave.model.DeviceRun;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设备运行表 Mapper 接口
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface DeviceRunMapper extends BaseMapper<DeviceRun> {

}
