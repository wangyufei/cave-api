package com.ydrobot.cave.common.api;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * 分页数据封装类 Created by macro on 2019/4/19.
 */
public class CommonPage<T> {
	private Long total;
	private List<T> list;

	/**
	 * 将MyBatis Plus 分页结果转化为通用结果
	 */
	public static <T> CommonPage<T> restPage(Page<T> pageResult) {
		CommonPage<T> result = new CommonPage<>();
		result.setTotal(pageResult.getTotal());
		result.setList(pageResult.getRecords());
		return result;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}
}
