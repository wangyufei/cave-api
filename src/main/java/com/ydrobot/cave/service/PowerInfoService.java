package com.ydrobot.cave.service;

import com.ydrobot.cave.model.PowerInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 电源信息表 服务类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface PowerInfoService extends IService<PowerInfo> {

}
