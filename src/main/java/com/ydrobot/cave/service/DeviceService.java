package com.ydrobot.cave.service;

import com.ydrobot.cave.model.Device;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设备信息表 服务类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface DeviceService extends IService<Device> {

}
