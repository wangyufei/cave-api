package com.ydrobot.cave.service.impl;

import com.ydrobot.cave.model.PowerInfo;
import com.ydrobot.cave.mapper.PowerInfoMapper;
import com.ydrobot.cave.service.PowerInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 电源信息表 服务实现类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@Service
public class PowerInfoServiceImpl extends ServiceImpl<PowerInfoMapper, PowerInfo> implements PowerInfoService {

}
