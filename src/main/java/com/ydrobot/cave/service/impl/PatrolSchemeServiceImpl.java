package com.ydrobot.cave.service.impl;

import com.ydrobot.cave.model.PatrolScheme;
import com.ydrobot.cave.mapper.PatrolSchemeMapper;
import com.ydrobot.cave.service.PatrolSchemeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡检计划表 服务实现类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@Service
public class PatrolSchemeServiceImpl extends ServiceImpl<PatrolSchemeMapper, PatrolScheme> implements PatrolSchemeService {

}
