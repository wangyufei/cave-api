package com.ydrobot.cave.service.impl;

import com.ydrobot.cave.model.Device;
import com.ydrobot.cave.mapper.DeviceMapper;
import com.ydrobot.cave.service.DeviceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设备信息表 服务实现类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@Service
public class DeviceServiceImpl extends ServiceImpl<DeviceMapper, Device> implements DeviceService {

}
