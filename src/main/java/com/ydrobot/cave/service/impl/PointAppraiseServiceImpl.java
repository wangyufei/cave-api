package com.ydrobot.cave.service.impl;

import com.ydrobot.cave.model.PointAppraise;
import com.ydrobot.cave.mapper.PointAppraiseMapper;
import com.ydrobot.cave.service.PointAppraiseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部件评价记录表 服务实现类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@Service
public class PointAppraiseServiceImpl extends ServiceImpl<PointAppraiseMapper, PointAppraise> implements PointAppraiseService {

}
