package com.ydrobot.cave.service.impl;

import com.ydrobot.cave.model.Defect;
import com.ydrobot.cave.mapper.DefectMapper;
import com.ydrobot.cave.service.DefectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设备缺陷表 服务实现类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@Service
public class DefectServiceImpl extends ServiceImpl<DefectMapper, Defect> implements DefectService {

}
