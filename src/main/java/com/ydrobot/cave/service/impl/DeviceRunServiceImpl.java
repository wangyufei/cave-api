package com.ydrobot.cave.service.impl;

import com.ydrobot.cave.model.DeviceRun;
import com.ydrobot.cave.mapper.DeviceRunMapper;
import com.ydrobot.cave.service.DeviceRunService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设备运行表 服务实现类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
@Service
public class DeviceRunServiceImpl extends ServiceImpl<DeviceRunMapper, DeviceRun> implements DeviceRunService {

}
