package com.ydrobot.cave.service;

import com.ydrobot.cave.model.PatrolScheme;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检计划表 服务类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface PatrolSchemeService extends IService<PatrolScheme> {

}
