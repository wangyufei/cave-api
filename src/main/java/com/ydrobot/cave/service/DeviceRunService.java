package com.ydrobot.cave.service;

import com.ydrobot.cave.model.DeviceRun;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设备运行表 服务类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface DeviceRunService extends IService<DeviceRun> {

}
