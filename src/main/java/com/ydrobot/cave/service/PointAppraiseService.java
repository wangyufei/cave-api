package com.ydrobot.cave.service;

import com.ydrobot.cave.model.PointAppraise;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 部件评价记录表 服务类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface PointAppraiseService extends IService<PointAppraise> {

}
