package com.ydrobot.cave.service;

import com.ydrobot.cave.model.Defect;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设备缺陷表 服务类
 * </p>
 *
 * @author wyf
 * @since 2021-01-19
 */
public interface DefectService extends IService<Defect> {

}
